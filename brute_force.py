import tools
from tools import Fields


class Engine:
    def __init__(self):
        self.x = 0
        self.y = 0
        self.y_possible_combinations = []
        self.x_possible_combinations = []
        self.nonogram = []

    def restart_coordinates(self):
        self.x = 0
        self.y = 0

    def restart_engine(self):
        self.x = 0
        self.y = 0
        self.y_possible_combinations = []
        self.x_possible_combinations = []
        self.nonogram = []

    def get_y_line(self):
        line = []
        for y in range(self.nonogram.y_dimension):
            line.append(self.nonogram.map[y][self.x])
        return line

    def get_x_line(self):
        line = []
        for x in range(self.nonogram.x_dimension):
            line.append(self.nonogram.map[self.y][x])
        return line

    def is_combination_possible(self):
        line = self.get_y_line()
        flag = False
        for y_line in self.y_possible_combinations[self.x]:
            for i in range(len(y_line)):
                if line[i] != Fields.unknown and y_line[i] != Fields.unknown and line[i] != y_line[i]:
                    break
                if i == len(y_line) - 1:
                    flag = True
                    break
            if flag:
                break
        if not flag:
            return False

        line = self.get_x_line()
        for x_line in self.x_possible_combinations[self.y]:
            for i in range(len(x_line)):
                if line[i] != Fields.unknown and x_line[i] != Fields.unknown and line[i] != x_line[i]:
                    break
                if i == len(x_line) - 1:
                    return True
        return False

    def set_next_field_value(self):
        if self.nonogram.map[self.y][self.x] == Fields.filled:
            self.nonogram.map[self.y][self.x] = Fields.empty

        elif self.nonogram.map[self.y][self.x] == Fields.empty:
            self.nonogram.map[self.y][self.x] = Fields.unknown
            self.x, self.y = tools.go_to_previous_field(self.x, self.y, self.nonogram.x_dimension)
            self.set_next_field_value()

    def solve(self, nonogram):
        self.nonogram = nonogram
        self.make_possible_combinations()
        self.restart_coordinates()
        self.nonogram.map[self.y][self.x] = Fields.filled

        while True:
            if self.is_combination_possible():
                self.x, self.y = tools.go_to_next_field(self.x, self.y, self.nonogram.x_dimension,
                                                        self.nonogram.y_dimension)
                if self.x == -1:
                    break
                self.nonogram.map[self.y][self.x] = Fields.filled
            else:
                self.set_next_field_value()

        return self.nonogram.map

    def make_possible_combinations(self):
        for horizontal_clues_line in self.nonogram.clues_horizontal:
            fields = tools.empty_places_to_put_soft_spaces(horizontal_clues_line)
            soft_spaces_count = tools.count_available_soft_spaces(self.nonogram.y_dimension, horizontal_clues_line)
            soft_spaces_unique_permutations = tools.make_all_unique_permutations(soft_spaces_count, fields)
            self.y_possible_combinations.append(tools.make_line(soft_spaces_unique_permutations, horizontal_clues_line))

        for vertical_clues_line in self.nonogram.clues_vertical:
            fields = tools.empty_places_to_put_soft_spaces(vertical_clues_line)
            soft_spaces_count = tools.count_available_soft_spaces(self.nonogram.x_dimension, vertical_clues_line)
            soft_spaces_unique_permutations = tools.make_all_unique_permutations(soft_spaces_count, fields)
            self.x_possible_combinations.append(tools.make_line(soft_spaces_unique_permutations, vertical_clues_line))
