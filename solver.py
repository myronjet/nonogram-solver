import brute_force
import tools
import gui


def resolve_and_show(x_dimension, y_dimension, resolved_map):
    gui_board = gui.Application(x_dimension, y_dimension)
    for y in range(y_dimension):
        for x in range(x_dimension):
            gui_board.modify(x, y, True if resolved_map[y][x] == tools.Fields.filled else False)
    gui_board.mainloop()

if __name__ == '__main__':
    # clues_horizontal_1 = [1, 2], [3], [1, 2], [3], [4]
    # clues_vertical_1 = [1], [1, 1], [2, 2], [5], [4]
    # clues_horizontal_2 = [3, 1], [4, 2], [1], [3, 3], [1, 2, 1], [1, 3], [2, 1]
    # clues_vertical_2 = [2, 3], [2, 1], [2, 2, 1], [1, 1, 1], [2, 1], [1, 1, 2], [2, 3]
    # clues_horizontal_3 = [2, 1], [1, 2, 1], [1, 1, 1, 1, 2], [1, 1, 2, 1], [1, 1, 1, 1, 1], [1, 3, 1], [1, 1, 1, 3], [1, 1, 3], [1, 1, 1], [2]
    # clues_vertical_3 = [2, 3], [1, 1, 1], [1, 1, 3], [2, 2], [3, 2, 2], [1, 1, 1, 1], [3, 2], [2, 2], [1, 1], [5]
    # clues_horizontal_4 = [2, 1, 2], [4, 1], [2, 1, 2], [2, 2, 2], [3, 1], [1, 2, 5], [1, 3], [1, 1, 1, 1], [1, 2, 2], [5]
    # clues_vertical_4 = [1, 2, 1], [5, 1, 1], [2, 4, 1], [1, 1, 1, 1], [2, 1, 2], [4, 2], [1, 1, 2], [1, 1, 3], [2, 1, 1], [2, 1, 2]
    # clues_horizontal_5 = [3, 1, 6], [3, 4, 1, 2], [1, 2, 1, 1, 2], [4, 1, 1, 2], [2, 3, 1, 1], [1, 1, 7], [1, 1, 2, 3, 1], [4, 1, 1, 1], [3, 3, 1, 2], [1, 1, 1, 3, 1], [1, 3, 2, 3], [7, 1], [3, 2, 1, 1], [3, 2, 2, 1], [5, 1, 3]
    # clues_vertical_5 = [5, 1, 1, 1], [1, 2, 1, 2, 2], [2, 1, 2, 2], [1, 1, 1, 1, 1, 2], [3, 1, 6, 1], [2, 1, 2, 2, 1], [2, 2, 7], [3, 2, 4], [2, 1], [2, 1, 1, 1, 2, 1], [1, 1, 3, 3, 2], [2, 3, 1, 2], [2, 2, 2, 1], [1, 2, 1, 5], [1, 3, 5]
    clues_horizontal_6 = [3, 1, 3, 1, 1, 1, 2], [3, 1, 3, 2, 1, 1, 3], [3, 2, 2, 2, 6], [3, 2, 2, 1, 2, 1], [3, 3, 2, 1, 2, 1, 1], [1, 1, 3, 2, 1, 2, 2, 1], [3, 3, 1, 2, 1, 1, 2, 1], [1, 5, 4, 2, 2], [1, 5, 4, 2, 2, 1], [1, 3, 1, 3, 5, 1], [1, 3, 1, 1, 3, 7], [1, 1, 3, 1, 1, 6, 5], [3, 2, 1, 2, 1, 1, 4], [2, 1, 2, 1, 2, 2, 4], [2, 2, 3, 6, 4], [2, 1, 1, 2, 6, 3], [1, 1, 1, 3, 1, 5], [1, 1, 3, 3, 1, 5], [1, 1, 4, 3, 1, 5], [1, 1, 4, 1, 1, 8], [1, 1, 2, 1, 1, 4, 2], [2, 1, 3, 2, 1, 2, 5], [2, 1, 3, 1, 1, 1, 1, 4], [2, 1, 3, 2, 1, 2, 3], [2, 1, 3, 2, 2, 3]
    clues_vertical_6 = [16, 8], [5, 1, 5, 4], [7, 2], [12], [12, 1], [12], [12, 4], [2, 2, 1, 8], [12, 11], [6, 1, 3], [4, 4, 2], [4], [3, 8], [18], [6, 10], [2, 9], [3, 1, 3, 2], [1, 8, 2], [11, 4, 1], [6, 4, 5], [1, 6, 9], [2, 15, 4], [25], [3, 13], [3, 4, 1, 1]

    brute_force_engine = brute_force.Engine()

    # nonogram1 = tools.Nonogram(clues_horizontal_1, clues_vertical_1)
    # nonogram2 = tools.Nonogram(clues_horizontal_2, clues_horizontal_2)
    # nonogram3 = tools.Nonogram(clues_horizontal_3, clues_horizontal_3)
    # nonogram4 = tools.Nonogram(clues_horizontal_4, clues_vertical_4)
    # nonogram5 = tools.Nonogram(clues_horizontal_5, clues_vertical_5)
    nonogram6 = tools.Nonogram(clues_horizontal_6, clues_vertical_6)

    # resolved_map1 = brute_force_engine.solve(nonogram1)
    # resolved_map2 = brute_force_engine.solve(nonogram2)
    # resolved_map3 = brute_force_engine.solve(nonogram3)
    # resolved_map4 = brute_force_engine.solve(nonogram4)
    # resolved_map5 = brute_force_engine.solve(nonogram5)
    resolved_map6 = brute_force_engine.solve(nonogram6)

    resolve_and_show(len(clues_horizontal_6), len(clues_vertical_6), resolved_map6)




