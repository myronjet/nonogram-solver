from enum import Enum
import itertools


class Fields(Enum):
    unknown = 0
    empty = 1
    filled = 2


class Nonogram:
    def __init__(self, clues_horizontal, clues_vertical):
        self.clues_horizontal = clues_horizontal
        self.clues_vertical = clues_vertical
        self.x_dimension = len(clues_horizontal)
        self.y_dimension = len(clues_vertical)
        self.map = [[Fields.unknown for y in range(self.y_dimension)] for x in range(self.x_dimension)]


def go_to_next_field(x, y, x_dimension, y_dimension):
    x += 1
    if x >= x_dimension:
        x = 0
        y += 1
    if y >= y_dimension:
        x = -1
        y = -1
    return x, y


def go_to_previous_field(x, y, x_dimension):
    x -= 1
    if x < 0:
        x = x_dimension - 1
        y -= 1
    if y < 0:
        x = -1
        y = -1
    return x, y


def count_clues_values_sum(clue_numbers):
    clues_values_sum = 0
    for clue_number in clue_numbers:
        clues_values_sum += clue_number
    return clues_values_sum


def required_hard_spaces(clue_numbers):
    return len(clue_numbers) - 1


def empty_places_to_put_soft_spaces(clue_numbers):
    return len(clue_numbers) - 1 + 2


def count_available_soft_spaces(dimension, clue_numbers):
    return dimension - count_clues_values_sum(clue_numbers) - required_hard_spaces(clue_numbers)


def is_line_possible(clue_numbers, dimension):
    space_needed = 0
    space_needed += required_hard_spaces(clue_numbers)
    space_needed += count_clues_values_sum(clue_numbers)
    if space_needed > dimension:
        return False
    else:
        return True


class UniqueElement:
    def __init__(self, value, occurrences):
        self.value = value
        self.occurrences = occurrences


def perm_unique(elements):
    eset = set(elements)
    unique_list = [UniqueElement(i, elements.count(i)) for i in eset]
    u = len(elements)
    return perm_unique_helper(unique_list, [0] * u, u - 1)


def perm_unique_helper(unique_list, result_list, d):
    if d < 0:
        yield tuple(result_list)
    else:
        for i in unique_list:
            if i.occurrences > 0:
                result_list[d] = i.value
                i.occurrences -= 1
                for g in perm_unique_helper(unique_list, result_list, d - 1):
                    yield g
                i.occurrences += 1


def make_all_unique_permutations(soft_spaces_count, fields):
    combinations = make_all_combinations(soft_spaces_count, fields)
    returns = []
    for combination in combinations:
        returns.append(list(perm_unique(combination)))
    return list(itertools.chain.from_iterable(returns))


def make_all_combinations(soft_spaces_count, fields):
    returns = []
    possible_combinations = itertools.combinations_with_replacement(range(soft_spaces_count + 1), fields)
    for i in possible_combinations:
        if sum(i) == soft_spaces_count:
            returns.append(i)
    return returns


def create_filled_line(line, clues):
    filled_count = count_clues_values_sum(clues)
    for i in range(filled_count):
        line.append(Fields.filled)
    return line


def put_hard_spaces(clues, line):
    iterator = 0
    for i in range(len(clues) - 1):
        if i == 0:
            iterator += clues[i]
            line.insert(iterator, Fields.empty)
        else:
            iterator += clues[i] + 1
            line.insert(iterator, Fields.empty)
    return line


def put_soft_spaces(permutation, clues, line):
    iterator = 0
    clues_iterator = 0
    for i in range(len(permutation)):
        for _ in range(permutation[i]):
                line.insert(iterator, Fields.empty)
                iterator += 1
        if clues_iterator >= len(clues):
            break
        iterator += clues[clues_iterator] + 1
        clues_iterator += 1

    return line


def make_line(permutations, clues):
    lines = []
    for permutation in permutations:
        line = []
        line = create_filled_line(line, clues)
        line = put_hard_spaces(clues, line)
        line = put_soft_spaces(permutation, clues, line)
        lines.append(line)

    return lines
