import tkinter as tk

SIZE = 15

class Application(tk.Frame):
    def __init__(self, width=SIZE, height=SIZE, master=None):
        super().__init__(master)
        self.grid()
        self.width = width
        self.height = height
        self.create_widgets()

    def create_widgets(self):
        self.widgets = []
        for x in range(self.width):
            for y in range(self.height):
                w = tk.Button(self)
                w["bg"] = "white"
                w.configure(state=tk.DISABLED)
                w.grid(row=x, column=y)
                self.widgets.append(w)

    def modify(self, x, y, val):
        if val:
            col = "black"
        else:
            col = "white"
        self.widgets[y*self.width + x]["bg"] = col


