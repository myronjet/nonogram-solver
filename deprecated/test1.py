import numpy as np
import math

class Row:
    def __init__(self, verticalLine, dim):

        self.combinations = []
        self.strEndCell = 0
        self.numberOfNumbers = 0
        self.positionToShift = 0
        self.currentCombination = []
        self.positionHolded = 0
        self.lastCell = 0;
        self.dim = dim
        self.verticalLine = verticalLine
        self.numberOfNumbers = len(verticalLine)

        self.makeFirstCombination(0)
        self.makeCombinations()

        self.currentCombinationCounter = 0

    def makeCombinations(self):
        while self.isNextCombination():
            self.makeNextCombination()


    def makeFirstCombination(self, lastCell):
        self.lastCell = lastCell
        self.currentCombination = [0] * self.dim
        numberSum = 0

        for number in self.verticalLine:
            for i in range(0, number):
                self.currentCombination[lastCell] = 2
                lastCell += 1
            lastCell += 1
            numberSum += number
        self.positionHolded = self.numberOfNumbers + numberSum

        self.combinations.append(list(self.currentCombination))

    def isNextCombination(self):
        result = False
        position = self.dim - self.strEndCell - 1

        for posCounter in reversed(range(0, position - self.strEndCell + 1)):
            if self.currentCombination[posCounter] == 2:
                first = posCounter + 1
                second = self.dim - self.strEndCell
                if (posCounter + 1) < (self.dim - self.strEndCell):
                    if self.currentCombination[posCounter + 1] == 0:
                        if posCounter + 2 < self.dim - self.strEndCell:
                            if self.currentCombination[posCounter + 2] == 0:
                                self.positionToShift = posCounter
                                result = True
                                break
                        else:
                            self.positionToShift = posCounter
                            result = True
                            break

        if result == False and self.numberOfNumbers != 1:
            if self.dim - self.strEndCell*2 - self.positionHolded > 0:
                self.strEndCell += 1
                self.makeFirstCombination(self.strEndCell)
                self.makeCombinations()

        return result

    def makeNextCombination(self):
        position = self.positionToShift

        for posCounter in reversed(range(0, position + 1)):
            if self.currentCombination[posCounter] == 2:
                self.currentCombination[posCounter + 1] = 2
                self.currentCombination[posCounter] = 0
            else:
                break
        self.combinations.append(list(self.currentCombination))

    def getCurrentCombination(self):
        return self.combinations[self.currentCombinationCounter]
    def getCombinationCounter(self):
        return self.currentCombinationCounter
    def resetCombinationCounter(self):
        self.currentCombinationCounter = 0
    def increaseCombinationCounter(self):
        self.currentCombinationCounter += 1
    def getCombinationCount(self):
        return len(self.combinations)
# ----------------------------------------------------------------------------
class Column:
    def __init__(self, horizontalLine, dim):
        self.numberSum = 0
        self.numberCount = 0

        for value in horizontalLine:
            self.numberSum += value
            self.numberCount += 1
    def getNumberSum(self):
        return  self.numberSum
    def getNumberCount(self):
        return self.numberCount
# ----------------------------------------------------------------------------

def validateNonogram(map, cols, rowsCount):
    numberSum = None
    truesCount = None
    breakCount = None
    currentCol = None

    for col in cols:
        truesCount = 0
        numberSum = col.getNumberSum()
        for i in range(rowsCount):
            if(map[i][currentCol] == 2):
                truesCount += 1
                if i + 1 < rowsCount:
                    if map[i+1][currentCol] == 0:
                        for j in range(i+1, rowsCount):
                            if map[j][currentCol] == 2:
                                breakCount +=1
                                break
        if col.getNumberCount() == 1:
            if not(col.getNumberSum() == rowsCount):
                if breakCount > 1:
                    return False
        if not(truesCount == numberSum) or not(breakCount == col.getNumberCount() -1):
            return False
        currentCol += 1
        breakCount = 0
    return True

def solve(map, rows, cols):

    for y in range(len(rows)):
        combination = rows[y].getCurrentCombination()
        for x in range(len(cols)):
            map[y][x] = combination[x]
    n = len(rows)

    while(not validateNonogram(map, cols,len(rows))):
        it = n - 1;

        if (rows[n-1].getCombinationCounter() != rows[n-1].getCombinationCount()-1):
            rows[n-1].increaseCombinationCounter()
            combination = rows[n-1].getCurrentCombination()
            for k in range(len(cols)):
                map[n-1][k] = combination[k]
        else:
            while(True):
                if(it == 0):
                    raise Exception('noResult')

                rows[it].resetCombinationCounter()
                combination = rows[it].getCurrentCombination()
                for k in range(len(cols)):
                    map[it][k] = combination[k]
                test = rows[it-1].getCombinationCounter()
                test1 = rows[it-1].getCombinationCount()
                if rows[it-1].getCombinationCounter() != rows[it-1].getCombinationCount()-1: #
                    rows[it-1].increaseCombinationCounter() #
                    combination = rows[it-1].getCurrentCombination() #
                    for k in range(len(cols)):
                        map[it-1][k] = combination[k]
                else:
                    rows[it-1].increaseCombinationCounter()
                it -= 1

                if(rows[it].getCombinationCounter() != rows[it].getCombinationCount()):
                    break



if __name__ == '__main__':
    # -----------------------------MAPS--------------------------------------------
    #5x5
    cluesHorizontal1 = [1,2],[1,2],[2,1],[3],[1]
    cluesVertical1 = [3],[3],[1],[4],[2]
    #10x10
    cluesHorizontal2 = [8],[2,5],[2,1],[3],[2,1],[6],[7],[1,1,3],[2,2],[2,2]
    cluesVertical2 = [3,3],[3,2],[1,1],[1,3],[2,4],[2,1,2],[8],[2,1,3],[1,5],[2]
    #15x15
    cluesHorizontal3 = [4,2,3],[2,1,5],[1,1,9],[8],[1,1,4],[2],[2,3],[4,3,3],[4,4,4],[7,4],[2,6,3],[3,1],[4,3],[2,2],[1,1,3]
    cluesVertical3 = [1,1,1,7],[2,7],[3,6,2],[1,6,1],[1,1,2,1],[1,3],[3,3],[5,3,1],[1,3,3,1],[4,2],[4],[4,2],[3,4,1,1],[2,4,3],[1,8]

    # rows = []
    # cols = []
    # colDim = len(cluesHorizontal1)
    # rowDim = len(cluesVertical1)
    #
    # for rowLine in cluesHorizontal1:
    #     rows.append(Row(rowLine, rowDim))
    # for colLine in cluesVertical1:
    #     cols.append(Column(colLine, colDim))
    #
    # map = [[0 for x in range(rowDim)] for x in range(colDim)]
    #
    # try:
    #     solve(map, rows, cols)
    #     print(map)
    # except :
    #     print("Map has no result")
