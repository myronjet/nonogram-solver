import numpy as np
import math

class Engine:
    def __init__(self,cluesHorizontal, cluesVertical):
        self.cluesHorizontal = cluesHorizontal
        self.cluesVertical = cluesVertical

        #to set values
        self.colDim = len(cluesHorizontal)
        self.rowDim = len(cluesVertical)
        self.rowCounter = None
        self.colCounter = None
        self.map = [[0 for x in range(self.rowDim)] for x in range(self.colDim)]

        #to make combinations
        self.lastCell = 0
        self.currentCombination = [0]
        self.strEndCell = 0
        self.positionToShift = 0
        self.positionHolded = 0
        self.makeNewCombinationFlag = True

        #start
        print(self.map)
        self.solve()
        print(self.map)

    def setValue(self):
        if self.map[self.colCounter][self.rowCounter] == 0:
            self.map[self.colCounter][self.rowCounter] = 2
        elif self.map[self.colCounter][self.rowCounter] == 2:
            self.map[self.colCounter][self.rowCounter] = 1
        elif self.map[self.colCounter][self.rowCounter] == 1:
            self.map[self.colCounter][self.rowCounter] = 0
            if(self.rowCounter == 0):
                self.colCounter -= 1
                self.rowCounter = self.rowDim-1
            else:
                self.rowCounter -= 1
            self.setValue()

    def validateLine(self, dimention):
        if dimention == "row":
            line = self.cluesHorizontal[self.colCounter]
            position = self.colCounter
        elif dimention == "col":
            line = self.cluesVertical[self.rowCounter]
            position = self.rowCounter


    def solve(self):
        self.map[0][0] = 2
        help = 0
        while(True):
            help+= 1
            if(help == 524):
                print("bang")
            if self.validate(): #forward
                if(self.rowCounter == self.rowDim-1 and self.colCounter == self.colDim-1):
                    break #done
                if(self.rowCounter == self.rowDim-1):
                    self.rowCounter = 0
                    self.colCounter += 1
                else:
                    self.rowCounter += 1
            self.setValue()


    def makeFirstCombination(self, lastCell, dimention):
        self.lastCell = lastCell
        numberSum = 0

        if(dimention == "row"):
            line = self.cluesHorizontal[self.colCounter]
        else:
            line = self.cluesVertical[self.rowCounter]

        for number in line:
            for i in range(0, number):
                self.currentCombination[lastCell] = 2
                lastCell += 1
            lastCell += 1
            numberSum += number
        self.positionHolded = len(line) + numberSum


    def isNextCombinationFit(self, dimention):
        while self.isNextCombiation(dimention):

            if self.makeNewCombinationFlag:
                self.makeNextCombination()
            self.makeNewCombinationFlag = True

            if self.isCombinationFit(dimention):
                return True
        return False


    def isCombinationFit(self, dimention): #toUpgrade
        result = True

        if dimention == "row":
            for counter in range(self.rowDim):
                if self.map[self.colCounter][counter] == self.currentCombination[counter]:
                    continue
                elif self.map[self.colCounter][counter] == 0:
                    continue
                elif self.map[self.colCounter][counter] == 1 and \
                    self.currentCombination[counter] == 0:
                    continue
                else:
                    result = False
                    break

        elif dimention == "col":
            for counter in range(self.colDim):
                if self.map[counter][self.rowCounter] == self.currentCombination[counter]:
                    continue
                elif self.map[counter][self.rowCounter] == 0:
                    continue
                elif self.map[counter][self.rowCounter] == 1 and \
                    self.currentCombination[counter] == 0:
                    continue
                else:
                    result = False
                    break

        return result


    def isNextCombiation(self, dimention): # to change
        result = False

        if(dimention == "row"):
            dim = self.rowDim
            numberOfNumbers = len(self.cluesHorizontal[self.colCounter])
        elif(dimention == "col"):
            dim = self.colDim
            numberOfNumbers = len(self.cluesVertical[self.rowCounter])

        position = dim - self.strEndCell - 1

        for posCounter in reversed(range(0, position - self.strEndCell + 1)):
            if self.currentCombination[posCounter] == 2:
                first = posCounter + 1
                second = dim - self.strEndCell
                if (posCounter + 1) < (dim - self.strEndCell):
                    if self.currentCombination[posCounter + 1] == 0:
                        if posCounter + 2 < dim - self.strEndCell:
                            if self.currentCombination[posCounter + 2] == 0:
                                self.positionToShift = posCounter
                                result = True
                                break
                        else:
                            self.positionToShift = posCounter
                            result = True
                            break

        if result == False and numberOfNumbers != 1:
            if dim - self.strEndCell*2 - self.positionHolded > 0:
                self.strEndCell += 1
                self.currentCombination = [0] * dim
                self.makeFirstCombination(self.strEndCell, dimention)
                self.makeNewCombinationFlag = False
                result = True

        return result


    def makeNextCombination(self):
        position = self.positionToShift

        for posCounter in reversed(range(0, position + 1)):
            if self.currentCombination[posCounter] == 2:
                self.currentCombination[posCounter + 1] = 2
                self.currentCombination[posCounter] = 0
            else:
                break


    def validate(self):
        self.lastCell = 0
        self.strEndCell = 0
        self.currentCombination = [0] * self.rowDim
        self.makeFirstCombination(0, "row")
        if not self.isCombinationFit("row"):
            if not self.isNextCombinationFit("row"):
                return False

        self.lastCell = 0
        self.strEndCell = 0
        self.currentCombination = [0] * self.colDim
        self.makeFirstCombination(0, "col")
        if not self.isCombinationFit("col"):
            if not self.isNextCombinationFit("col"):
                return False

        return True


if __name__ == '__main__':
    # -----------------------------MAPS--------------------------------------------
    #5x5
    cluesHorizontal1 = [1,2],[1,2],[2,1],[3],[1]
    cluesVertical1 = [3],[3],[1],[4],[2]
    #10x10
    cluesHorizontal2 = [8],[2,5],[2,1],[3],[2,1],[6],[7],[1,1,3],[2,2],[2,2]
    cluesVertical2 = [3,3],[3,2],[1,1],[1,3],[2,4],[2,1,2],[8],[2,1,3],[1,5],[2]
    #15x15
    cluesHorizontal3 = [4,2,3],[2,1,5],[1,1,9],[8],[1,1,4],[2],[2,3],[4,3,3],[4,4,4],[7,4],[2,6,3],[3,1],[4,3],[2,2],[1,1,3]
    cluesVertical3 = [1,1,1,7],[2,7],[3,6,2],[1,6,1],[1,1,2,1],[1,3],[3,3],[5,3,1],[1,3,3,1],[4,2],[4],[4,2],[3,4,1,1],[2,4,3],[1,8]
    # ----------------------------------------------------------------------------

    # ---------------------------INFO----------------------------------------------
    # 0 - unknown      1- tryWhite      2 - tryBlack    3 - white     4- black #
    # ----------------------------------------------------------------------------


    #Engine(cluesHorizontal2, cluesVertical2)

    line =  [1,2,1]
    lineDimention = 10
    numerOfNumbers = 0
    numberSum = 0

    for values in line:
        numerOfNumbers += 1
        numberSum += values

    placesToPutSoftSpaces = numerOfNumbers + 1
    numberOfHardSpaces = numerOfNumbers - 1
    numberOfSoftSpaces = numberOfHardSpaces + numberSum
    combinationCount = (math.factorial(numberOfSoftSpaces + placesToPutSoftSpaces - 1))/(math.factorial(numberOfSoftSpaces)*math.factorial(placesToPutSoftSpaces-1))

    print("Number of hard spaces:", numberOfHardSpaces)
    print("Places to put soft spaces", placesToPutSoftSpaces)
    print("Nuber of soft spaces:",numberOfSoftSpaces)
    print("Combination count:", combinationCount)

    freeSpacesDepo = [0 for x in range(placesToPutSoftSpaces)]





